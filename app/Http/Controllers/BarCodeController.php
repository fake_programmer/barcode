<?php

namespace App\Http\Controllers;

use App\Models\Motor;
use Illuminate\Http\Request;

class BarCodeController extends Controller
{
    public function index()

    {
        $motors=Motor::all();

        return view('barcode', compact('motors'));

    }
    public function motorList($id)

    {
        $motors_info = Motor::where('id',$id)->get();
         $motors=Motor::all();

        return view('barcode', compact('motors','motors_info'));

    }

}
