<?php

namespace App\Http\Controllers;

use App\Models\Motor;
use Illuminate\Http\Request;

class MotorController extends Controller
{
    public function index()
    {
        return view('registration');
//        $motors= Motor::all();
////        dd($motors);
//        return view('barcode', compact('motors'));
    }

    public function insert(Request $request)
    {
//        dd($request->all());
        $test = Motor::create([
            'certificateno' => $request->input('certificateno'),
            'markandnumber' => $request->input('markandnumber'),
            'make' => $request->input('make'),
            'typeofbody' => $request->input('typeofbody'),
            'manufacture' => $request->input('manufacture'),
            'cubiccapacity' => $request->input('cubiccapacity'),
            'seatingcapacity' => $request->input('seatingcapacity'),
            'from' => $request->input('from'),
            'to' => $request->input('to'),
            'chasisno' => $request->input('chasisno'),
            'addrofinsured' => $request->input('addrofinsured'),
            'suminsured' => $request->input('suminsured'),
            'limitationastouse' => $request->input('limitationastouse'),
            'premium' => $request->input('premium'),
            'number' => $request->input('number'),
            'officeaddr' => $request->input('officeaddr'),
            'issue' => $request->input('issue'),
            'mrno' => $request->input('mrno')

        ]);
        session()->flash('message', 'Info Added Successfully');
        return redirect()->route('motor.list');


    }

    public function list(){
        $motorInformations = Motor::all();
        return view('motorlist', compact('motorInformations'));



    }

    public function preview($id)
    {
        $info = Motor::find($id);
        $motors_info = Motor::where('id',$id)->get();
        $motors=Motor::all();
//        dd($info);
        return view('preview', compact('info', 'motors_info', 'motors'));
    }





}
