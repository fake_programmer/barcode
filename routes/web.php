<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Route::get('/barcode', [\App\Http\Controllers\BarCodeController::class, 'index'])->name('barcode.index');
Route::get('/barcode/{id}', [\App\Http\Controllers\BarCodeController::class, 'motorList'])->name('barcode.index');
Route::get('/register', [\App\Http\Controllers\MotorController::class, 'index'])->name('motor.index');
Route::post('/register', [\App\Http\Controllers\MotorController::class, 'insert'])->name('motor.store');
Route::get('/motorList', [\App\Http\Controllers\MotorController::class, 'list'])->name('motor.list');
Route::get('/preview/{id}', [\App\Http\Controllers\MotorController::class, 'preview'])->name('preview');

//Route::get('/barcode', [\App\Http\Controllers\BarCodeController::class, 'index'])->name('barcode.index');
