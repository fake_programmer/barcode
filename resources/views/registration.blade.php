@extends('master')
@section('content')
    <div class="col-md-12">
        <form action="{{route('motor.store')}}" method="post" class="form-horizontal" role="form">
            @csrf
            <h1 class="text-center">South Asia Insurance Company Limited</h1>
            <h2 class="text-center">Certificate of Motor Insurance</h2>
            <div class="form-group">
                <label for="firstName" class="col-sm-3 control-label"> Insurance certificate no</label>
                <div class="col-sm-9">
                    <input type="text" name="certificateno" name="certificateno" placeholder=" " class="form-control" autofocus>
                </div>
            </div>
            <div class="form-group">
                <label for="lastName" class="col-sm-3 control-label"> Registration Mark and Number</label>
                <div class="col-sm-9">
                    <input type="text" name="markandnumber" placeholder="" class="form-control" autofocus>
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">make </label>
                <div class="col-sm-9">
                    <input type="text" name="make" placeholder=" " class="form-control" name="email">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-3 control-label">Type of body</label>
                <div class="col-sm-9">
                    <input type="text" name="typeofbody" placeholder=" " class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-3 control-label">Year of manufacture(model)</label>
                <div class="col-sm-9">
                    <input type="text" name="manufacture" placeholder="" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="birthDate" class="col-sm-3 control-label">cubic capacity</label>
                <div class="col-sm-9">
                    <input type="text" name="cubiccapacity" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="phoneNumber" class="col-sm-3 control-label">seating capacity </label>
                <div class="col-sm-9">
                    <input type="phoneNumber" name="seatingcapacity" placeholder="" class="form-control">

                </div>
            </div>
            <div class="form-group">
                <label for="Height" class="col-sm-3 control-label">period of insurance From </label>
                <div class="col-sm-9">
                    <input type="date" name="from" placeholder=""
                           class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="Height" class="col-sm-3 control-label">period of insurance To </label>
                <div class="col-sm-9">
                    <input type="date" name="to" placeholder=""
                           class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="weight" class="col-sm-3 control-label">Eng no. & Chasis no</label>
                <div class="col-sm-9">
                    <input type="number" name="chasisno" placeholder=""
                           class="form-control">
                </div>
            </div>
            <h2 class="text-center">Act liability</h2>

            <div class="form-group">
                <label for="weight" class="col-sm-3 control-label">Name and address of the insured</label>
                <div class="col-sm-9">
                    <input type="text" name="addrofinsured" placeholder=""
                           class="form-control">
                </div>
            </div> <!-- /.form-group -->

            <div class="form-group">
                <label for="weight" class="col-sm-3 control-label">Sum insured</label>
                <div class="col-sm-9">
                    <input type="text" name="suminsured" placeholder=""
                           class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="weight" class="col-sm-3 control-label">Limitation as to use</label>
                <div class="col-sm-9">
                    <input type="text" name="limitationastouse" placeholder=""
                           class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="weight" class="col-sm-3 control-label">premium</label>
                <div class="col-sm-9">
                    <input type="text" name="premium" placeholder=""
                           class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="weight" class="col-sm-3 control-label">Renewal Number</label>
                <div class="col-sm-9">
                    <input type="number" name="number" placeholder=""
                           class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="weight" class="col-sm-3 control-label">Issuing office address</label>
                <div class="col-sm-9">
                    <input type="text" name="officeaddr" placeholder=""
                           class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="weight" class="col-sm-3 control-label">Date of issue</label>
                <div class="col-sm-9">
                    <input type="date" name="issue" placeholder=""
                           class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="weight" class="col-sm-3 control-label">MR no & date</label>
                <div class="col-sm-9">
                    <input type="date" name="mrno" placeholder=""
                           class="form-control">
                </div>
            </div>

            <button type="submit" class="btn btn-primary btn-block">Register</button>
        </form> <!-- /form -->
    </div>


@endsection
