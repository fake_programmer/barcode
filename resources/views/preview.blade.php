@extends('master')
@section('content')
    <div class="col-md-12">
        {{--    {{$info}}--}}
        <div class="card">
            <div class="card-header text-center">
                <h1 class="text-uppercase">Bangladesh Co-Operative Insurance Limited</h1>
                <p> Head Office: saiham Sky view Tower, Dhaka</p>
                <p> Phone No: 0184569635</p>
                <h5 class="card-title">Certificate Of Motor Insurance</h5>
            </div>
            <div class="card-body">
                <ul class="list-group">

                    <li class="list-group-item">
                        <div class="col-md-6 float-left">
                        <span>
                            Insurance Certificate no:
                        </span>
                        </div>
                        <div class="col-md-6 float-left">
                         <span>
                             {{ $info->certificateno }}
                         </span>
                        </div>
                    </li>

                    <li class="list-group-item">
                        <div class="col-md-6 float-left">
                        <span>
                            Reg Mark or Number:
                        </span>
                        </div>
                        <div class="col-md-6 float-left">
                         <span>
                             {{ $info->markandnumber }}
                         </span>
                        </div>
                    </li>

                    <li class="list-group-item">
                        <div class="col-md-6 float-left">
                        <span>
                            Mark:
                        </span>
                        </div>
                        <div class="col-md-6 float-left">
                         <span>
                             {{ $info->make }}
                         </span>
                        </div>
                    </li>

                    <li class="list-group-item">
                        <div class="col-md-6 float-left">
                        <span>
                            Year of Manufacture:
                        </span>
                        </div>
                        <div class="col-md-6 float-left">
                         <span>
                             {{ $info->make }}
                         </span>
                        </div>
                    </li>

                    <li class="list-group-item">
                        <div class="col-md-6 float-left">
                        <span>
                            CC Capacity:
                        </span>
                        </div>
                        <div class="col-md-6 float-left">
                         <span>
                             {{ $info->cubiccapacity }}
                         </span>
                        </div>
                    </li>

                    <li class="list-group-item">
                        <div class="col-md-6 float-left">
                        <span>
                            Seating Capacity:
                        </span>
                        </div>
                        <div class="col-md-6 float-left">
                         <span>
                             {{ $info->seatingcapacity }}
                         </span>
                        </div>
                    </li>


                    <li class="list-group-item">
                        <div class="col-md-6 float-left">
                        <span>
                            Period of Insurance:
                        </span>
                        </div>
                        <div class="col-md-6 float-left">
                         <span>
                             {{ $info->markandnumber }}
                         </span>
                        </div>
                    </li>


                    <li class="list-group-item">
                        <div class="col-md-6 float-left">
                        <span>
                            Sum insured:
                        </span>
                        </div>
                        <div class="col-md-6 float-left">
                         <span>
                             {{ $info->markandnumber }}
                         </span>
                        </div>
                    </li>
                </ul>
            </div>

        </div>

        <br>

        <div class="card">
            <div class="card-header">
                <h1>ACT Liability</h1>
                <hr>
            </div>
            <div class="card-body">
                <ul class="list-group">

                    <li class="list-group-item">
                        <div class="col-md-6 float-left">
                        <span>
                            Name & Address of the Insured:
                        </span>
                        </div>
                        <div class="col-md-6 float-left">
                         <span>
                             {{ $info->addrofinsured }}
                         </span>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="col-md-6 float-left">
                        <span>
                            Name & Address of the Insured:
                        </span>
                        </div>
                        <div class="col-md-6 float-left">
                         <span>
                             {{ $info->addrofinsured }}
                         </span>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="col-md-6 float-left">
                        <span>
                            Insured:
                        </span>
                        </div>
                        <div class="col-md-6 float-left">
                         <span>
                             {{ $info->addrofinsured }}
                         </span>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="col-md-6 float-left">
                        <span>
                            Limitation as to use:
                        </span>
                        </div>
                        <div class="col-md-6 float-left">
                         <span>
                             {{ $info->limitationastouse }}
                         </span>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="col-md-6 float-left">
                        <span>
                            Premium Computation :
                        </span>
                        </div>
                        <div class="col-md-6 float-left">
                         <span>
                             {{ $info->premium }}
                         </span>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="col-md-6 float-left">
                        <span>
                            Issue Office Address:
                        </span>
                        </div>
                        <div class="col-md-6 float-left">
                         <span>
                             {{ $info->officeaddr }}
                         </span>
                        </div>
                    </li>

                    <li class="list-group-item">
                        <div class="col-md-6 float-left">
                        <span>
                            Issue Date:
                        </span>
                        </div>
                        <div class="col-md-6 float-left">
                         <span>
                             {{ $info->issue }}
                         </span>
                        </div>
                    </li>

                </ul>

            </div>

        </div>
        <button class="print-window btn btn-sm btn-primary">Print</button>




{{--        // BarCode Blade--}}


        <div class="container text-center">

            <div class="row">

                <div class="col-md-8 offset-md-2">
                    {{--{{dd($motors_info)}}--}}
                    @foreach($motors_info as $motors_infos)
                        <div class="col-md-3">

                        </div>

                        <div class="col-md-3">{!! DNS2D::getBarcodeHTML($motors_infos->certificateno. ' Registration No '
            .$motors_infos->markandnumber. ''.$motors_infos->make,
            'QRCODE')
            !!}</div>

                        </br>
                    @endforeach
                </div>

            </div>

        </div>












    </div>
@endsection
@push('js')
    <script>
        $('.print-window').click(function () {
            window.print();
        });
    </script>
@endpush
