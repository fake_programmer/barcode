@extends('master')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <h1 class="text-center">South Asia Insurance Company Limited</h1>
            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">First</th>
                        <th scope="col">Last</th>
                        <th scope="col">Handle</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 1;
                    ?>
                    @foreach($motorInformations as $data)
                        <tr>
                            <th scope="row"></th>
                            <td> {{$i++}}</td>
                            <td>{{$data->certificateno}}</td>
                            <td>{{$data->markandnumber}}</td>
                            <td>{{$data->make}}</td>
                            <td>{{$data->typeofbody}}</td>
                            <td style="text-align: center;">
                                <a href="{{route('barcode.index', $data->id)}}">
                                    <span class="icon edit_icon">
                                        <button type="button" class="btn btn-info">GetQR</button>
                                    </span>
                                </a>
                                <a href="{{route('preview',$data->id)}}">
                                    <span class="icon edit_icon">
                                        <button type="button" class="btn btn-info">Preview</button>
                                    </span>
                                </a>


                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
