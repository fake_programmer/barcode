<!DOCTYPE html>

<html>

<head>

    <title>South Asia Insurance Company Limited</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

</head>

<body>

<div class="card">
    <h1 class="text-center">South Asia Insurance Company Limited</h1>
    <div class="card-body">
        <div class="container text-center">

            <div class="row">

                <div class="col-md-8 offset-md-2">
                    {{--{{dd($motors_info)}}--}}
                    @foreach($motors_info as $motors_infos)
                        <div class="col-md-3">

                        </div>

                        <div class="col-md-3">{!! DNS2D::getBarcodeHTML($motors_infos->certificateno. ' Registration No '
            .$motors_infos->markandnumber. ''.$motors_infos->make,
            'QRCODE')
            !!}</div>

                        </br>
                    @endforeach
                </div>

            </div>

        </div>

    </div>

</div>

</body>

</html>
