<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMotorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('motors', function (Blueprint $table) {
            $table->id();
            $table->integer('certificateno');
            $table->string('markandnumber');
            $table->string('make');
            $table->string('typeofbody');
            $table->string('manufacture');
            $table->string('cubiccapacity');
            $table->string('seatingcapacity');
            $table->date('from');
            $table->date('to');
            $table->string('chasisno');
            $table->string('addrofinsured');
            $table->string('suminsured');
            $table->string('limitationastouse');
            $table->string('premium');
            $table->string('number');
            $table->string('officeaddr');
            $table->string('issue');
            $table->string('mrno');






            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('motors');
    }
}
